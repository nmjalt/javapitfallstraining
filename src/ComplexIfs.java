
public class ComplexIfs {

    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        int c = Integer.parseInt(args[2]);

        if( a<2 ) {
            if( c>0 ) {
                if( b>100 ) {
                    System.out.println("Iznākums 1");
                } else if( a < 1) {
                    System.out.println("Iznākums 2");
                }
            } else {
                System.out.println("Iznākums 3");
            }
        } else if( c>0 && b>100 ) {
            System.out.println("Iznākums 1");
        }

    }

}
