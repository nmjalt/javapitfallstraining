package com.company;

public class ImportantBusinessClass {

    private String content1 = "something";

    private OtherBusinessClass otherBusinessObject = new OtherBusinessClass();

    public String expensiveOperation() {
        System.err.println("something expensive happens here");
        return "outcome of expensive operation";
    }

    public String represent() {
        return expensiveOperation() +"--"+ otherBusinessObject.otherCall();
    }

    @Override
    public String toString() {
        return "ImportantBusinessClass{" +
                "content1='" + content1 + '\'' +
                ", otherBusinessObject=" + otherBusinessObject.getCalculationResult() +
                '}';
    }

}
