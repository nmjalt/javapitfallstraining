package com.company;

public class OtherBusinessClass {

    int calculationResult =0;

    public int otherCall() {
        return ++calculationResult;
    }

    public int getCalculationResult() {
        return calculationResult;
    }

    @Override
    public String toString() {
        return "represent:" + otherCall();
    }

}
