package com.company;

import java.util.List;

public final class ShippingDataService {

    public static final ShippingDataService INSTANCE = new ShippingDataService();

    private ShippingDataService() {}

    private List<ImportantBusinessClass> dataList;

    private String outcome;

    public void setData(List<ImportantBusinessClass> dataList) {
        this.dataList = dataList;
    }

    public void process() {
        // do something
        outcome = dataList.stream().findFirst().toString();
    }

    public String getOutcome() {
        return outcome;
    }

}
